/*
function cambiarModo(){
    var cuerpoweb = document.body;
    cuerpoweb.classList.toggle("oscuro");
}
*/

$('.boton_toggle').on("click", function(){//al pulsar el botón, cambia la clase del menú y el icono
    $('.boton_toggle').toggleClass('boton_toggle_click');
    $('.menu_lateral').toggleClass('menu_lateral_click');
    $('.menu_lateral li a').css("color","white");
}
)
$('.menu_lateral li a').on("click", function(){ //al hacer click en un enlace, cambia de color simulando una selección, los demás se quedan blancos
    $('.menu_lateral li a').css("color","white");
    $(this).css("color","black");
})


$('.eng').css("display","none"); //la página empieza ocultando todo el texto en inglés y portugués
$('.por').css("display","none"); 

$('.enlaces_nav i').on("click", function(){//al hacer click en una de las banderas, se seleccionará y cambiará el texto español o inglés a oculto
    if($(this).hasClass('active')){
    }else{
        $('.enlaces_nav i').removeClass('active');
        $(this).addClass('active');
        if($(this).hasClass('spanish')){
            $('.esp').css("display","");
            $('.eng').css("display","none");
            $('.por').css("display","none");
        }else if($(this).hasClass('english')){
            $('.eng').css("display","");
            $('.esp').css("display","none"); 
            $('.por').css("display","none");    
        }else if($(this).hasClass('portuguese')){
            $('.por').css("display","");
            $('.esp').css("display","none"); 
            $('.eng').css("display","none"); 
        }
    }
    }
    )

    //Flecha para subir arriba

    $(document).ready(function(){ //Todo lo que escribamos dentro se va a ejecutar hasta que nuestro documento haya cargado

        $('.ir-arriba').click(function(){ //Para acceder a la clase ir arriba
            $('body,html').animate({
                scrollTop: '0px' //Para que el botón nos lleve a la parte de arriba
            },2000);
        }); 

        $(window).scroll(function(){ //Cuando hagamos scroll en la ventana va a recibir una función para que aparezca el botón
            if( $(this).scrollTop() > 0 ){
                $('.ir-arriba').slideDown(500);
            }else{
                $('.ir-arriba').slideUp(500);
            }
        });

    });